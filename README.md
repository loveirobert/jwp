# JayStack Website #

### Installation steps ###

1. Install dev. environment (wamp)
1. Create a database with jaystack name
1. Get wordpress from: https://en-ca.wordpress.org/wordpress-4.3-en_CA.zip
1. Copy the content of wordpress folder from the archive into the www folder of wamp
1. Install wordpress via http:\\localhost
1. Remove the contents of: wp-content\plugins\
1. Type git init in the www folder
1. Add git repository as remote origin with: git remote add origin [repourl]
1. Type git pull origin master
1. On Wp admin (http:\\localhost\wp-admin) go to plugins and activate jaystack plugin
1. You should see now a JayImportExport menu. Click it
1. Click import data (long process, takes 2-3 min. at least). Don't navigate away! Session expiration is not a problem. Wait while 'Wait for import' text disappeares!
1. Refresh the admin
1. Check main page