<?php
/*
Plugin Name: JayStack
Plugin URI: http://www.jaystack.com/
Description: Plugin to activate JayStack deployment possibilities
Version: 1.0
Author: Robert Lovei
Author URI: http://www.jaystack.com/
License: GPL
*/

include 'lib/backup-handler/export.php';
include 'lib/backup-handler/import.php';
include 'lib/backup-handler/admin-menu.php';