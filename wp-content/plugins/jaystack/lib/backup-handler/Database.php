<?php

class Database{
    protected $wpdb;
    protected $prefix;
    protected $domain;
    protected $importDomain;


    protected $tables = array(
        'options',
        'commentmeta',
        'comments',
        'links',
        'postmeta',
        'posts',
        'term_relationships',
        'term_taxonomy',
        'terms',
        'usermeta',
        'users',
        'layerslider',
        'revslider_css',
        'revslider_layer_animations',
        'revslider_navigations',
        'revslider_sliders',
        'revslider_slides',
        'revslider_static_slides',
        
    );
    
    protected $selectFilterMethods = array();
    protected $importFilters = array();
            
    function __construct(){
        global $wpdb;
        
        $this->wpdb = $wpdb;
        $this->prefix = $wpdb->prefix;
        
        $tablesLength = sizeof($this->tables);
        
        for($i = 0; $i < $tablesLength; $i++){
            $baseTableName = $this->tables[$i];
            $fullTableName = $this->prefix . $baseTableName;
            
            $this->selectFilterMethods[$fullTableName] = $baseTableName . 'ExportFilter';
            $this->importFilters[$fullTableName] = $baseTableName . 'ImportFilter';
            
            $this->tables[$i] = $fullTableName;
        }
        
        $this->domain = $_SERVER['HTTP_ORIGIN'];
    }
    
    protected function extractDomainFromOptionsImport($options){
        $optionsLength = sizeof($options);
        for($i = 0; $i < $optionsLength; $i++){
            $currentOption = $options[$i];
            if($currentOption->option_name == 'siteurl'){
                $this->importDomain = $currentOption->option_value;
                break;
            }
        }
    }
    
    protected function optionsImportFilter($options){
        $this->extractDomainFromOptionsImport($options);
    }
    
    protected function optionsExportFilter(){
        $filter = 'WHERE option_name NOT LIKE "%_transient_%"';
        return $filter;
    }
    
    protected function postmetaExportFilter(){
        $filter = 'WHERE meta_key NOT LIKE "_edit_%"';
        return $filter;
    }
    
    protected function postsExportFilter(){
        $filter = 'WHERE post_type <> "revision"';
        return $filter;
    }
}

