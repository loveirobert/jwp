<?php

function jay_import_export_menu(){
    ?>
        <div class="wrap">
            <h1 style="margin-bottom: 15px;">JayImportExport</h1>
            <button class="button" id="jay-import-all-button">Import Data</button>
            <button class="button" id="jay-export-all-button">Export Data</button>
            <div id="jay-message"></div>
        </div>
        <script type="text/javascript">
            (function($){
                $('#jay-import-all-button').click(function(){
                    jQuery('#jay-message').html('<b><br/>Wait for import!</b>');
                    $.ajax({
                            url: '/?import=true',
                            method: 'POST' 
                    }).done(function(results){
                        jQuery('#jay-message').html('');
                    });
                });
                $('#jay-export-all-button').click(function(){
                    jQuery('#jay-message').html('<b><br/>Wait for export!</b>');
                    $.ajax({
                            url: '/?export=true',
                            method: 'POST' 
                    }).done(function(results){
                        jQuery('#jay-message').html('');
                    });
                });
            })(jQuery)
        </script>
    <?php
}

function add_jay_import_export_menu(){
    add_menu_page('JayImportExport', 'JayImportExport', 'manage_options', 'jay_import_export', jay_import_export_menu);
}

add_action( 'admin_menu', 'add_jay_import_export_menu' );