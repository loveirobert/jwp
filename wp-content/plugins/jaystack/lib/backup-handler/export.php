<?php

include 'Database.php';

class DataExporter extends Database{
    public function __construct() {
        parent::__construct();
    }
    public function exportData(){
        $tablesLength = sizeof($this->tables);
        
        for($i = 0; $i < $tablesLength; $i++){
            $tableSelectFilter = '';
            $tableName = $this->tables[$i];
            $filterFunction = $this->selectFilterMethods[$tableName];
            
            if(method_exists($this, $filterFunction) == true){
                $tableSelectFilter = $this->$filterFunction();
            }
            
            $selectQuery = 'SELECT * FROM ' . $tableName . ' ' . $tableSelectFilter;
            
            $tableContent = $this->wpdb->get_results($selectQuery);
            
            $jsonContentToSave = json_encode($tableContent, JSON_PRETTY_PRINT);
            
            $fp = fopen('backup/' . $tableName . '.json', 'w');
            fwrite($fp, $jsonContentToSave);
            fclose($fp);
        }
    }
}

if(isset($_GET['export']) && $_GET['export'] === 'true'){
    $dataExport = new DataExporter();
    $dataExport->exportData();
    die();
}