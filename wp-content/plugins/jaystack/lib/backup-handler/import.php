<?php

include_once 'Database.php';

class DataImporter extends Database{
    public function __construct() {
        parent::__construct();
    }
    public function importData(){
        $tablesLength = sizeof($this->tables);
        
        for($i = 0; $i < $tablesLength; $i++){
            $currentTableName = $this->tables[$i];
            $currentTableData = json_decode(file_get_contents('backup/' . $currentTableName . '.json'));
            
            $filterFunction = $this->importFilters[$currentTableName];
            
            if(method_exists($this, $filterFunction) == true){
                $this->$filterFunction($currentTableData);
            }
            
            $this->insertData($currentTableName, $currentTableData);
        }
        
        $this->changeDomain($this->importDomain, $this->domain);
    }
    
    private function changeDomain($from, $to){
        $this->wpdb->query('UPDATE wp_options SET option_value = replace(option_value, "' . $from . '", "' . $to . '") WHERE option_name = "home" OR option_name = "siteurl"');
        $this->wpdb->query('UPDATE wp_posts SET guid = replace(guid, "' . $from . '", "' . $to . '")');
        $this->wpdb->query('UPDATE wp_posts SET post_content = replace(post_content, "' . $from . '", "' . $to . '")');
    }

    public function recreateTables(){
        $queryString = file_get_contents('backup/backup.sql');
        $queryStringLine = str_replace(array("\n", "\r"), array(" ", " "), $queryString);
        
        $queries = explode(';', $queryStringLine);
        
        $queriesLength = sizeof($queries);
        
        for($i = 0; $i < $queriesLength; $i++){
            $query = $queries[$i];
            $this->wpdb->query($query);
        }
    }
    private function insertData($tableName, $data){
        set_time_limit(60*60);
        $dataLength = sizeof($data);
        
        for($i = 0; $i < $dataLength; $i++){
            $currentData = $data[$i];
            $currentDataArray = (array) $currentData;
            $this->wpdb->insert(
                    $tableName,
                    $currentDataArray
            );
        }
    }
}
if(isset($_GET['import']) && $_GET['import'] === 'true'){
    $dataImporter = new DataImporter();
    $dataImporter->recreateTables();
    $dataImporter->importData();
    die();
}