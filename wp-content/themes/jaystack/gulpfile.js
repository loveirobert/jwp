//set PATH="c:\Ruby22-x64"\bin;%PATH%
var gulp = require('gulp'),     
    sass = require('gulp-ruby-sass') ,
    notify = require('gulp-notify') ,
    bower = require('gulp-bower'),
    compass = require('gulp-compass'),
    minifyCSS = require('gulp-minify-css');
 
var config = {
     sassPath: './scss',
     bowerDir: './bower_components' ,
    cssPath: './css'
};

gulp.task('bower', function() { 
    return bower()
         .pipe(gulp.dest(config.bowerDir)) 
});

gulp.task('icons', function() { 
    return gulp.src(config.bowerDir + '/fontawesome/fonts/**.*') 
        .pipe(gulp.dest('./fonts')); 
});

gulp.task('css', function () {
    
    sass(config.cssPath + '/site.scss',
        {
            style: 'compressed',
            loadPath: [
                './scss',
                config.bowerDir + '/bootstrap-sass/assets/stylesheets',
                config.bowerDir + '/fontawesome/scss',
            ]
        }
    )
    .pipe(gulp.dest('./css'));
    
});

 gulp.task('watch', function() {
     gulp.watch(config.sassPath + '/**/*.scss', ['css']); 
});

  gulp.task('default', ['bower', 'icons', 'css']);