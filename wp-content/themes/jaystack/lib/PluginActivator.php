<?php
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

class PluginActivator{
    private $pluginsOptions = 'active_plugins';
    
    public function __construct() {
        
    }
    
    public function activatePlugins($plugins){
        $pluginsLength = sizeof($plugins);
        for($i = 0; $i < $pluginsLength; $i++){
            $currentPlugin = $plugins[$i];
            if(is_plugin_active($currentPlugin) == false){
                $this->activatePlugin($currentPlugin);
            }
        }
    }
    
    private function activatePlugin($plugin){
        $currentPlugins = get_option($this->pluginsOptions);
        $currentPlugins[] = $plugin;
        update_option($this->pluginsOptions, $currentPlugins);
    }
}