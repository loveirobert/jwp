<?php
/*
 * activating base plugins
 */
include 'PluginActivator.php';

$requiredPlugins = array('jaystack/jaystack.php');
$pluginActivator = new PluginActivator();

$pluginActivator->activatePlugins($requiredPlugins);

/*
 * theme support options
 */
add_theme_support('menus');
add_theme_support('post-thumbnails');
