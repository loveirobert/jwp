(
    function($){
        $(document).ready(function(){
            $('nav .menu > li.menu-item-has-children').hover(function(){
                var menuOffsetLeft = $(this).offset().left;
                $(this).find('ul.sub-menu').css({display: 'block'});
                //var width = $(this).children('ul.sub-menu:eq(0)').outerWidth(true);
                var subsWidth = 0;
                $(this).children('ul').children('li').each(function(index, item){
                    subsWidth += $(this).outerWidth(false);
                });
                var margin = menuOffsetLeft - subsWidth / 2 + $(this).children('ul').children('li').length * 13;
                $(this).find('ul.sub-menu').css({display: 'none'});
                $(this).children('ul').children('li:eq(0)').css({marginLeft: margin + 'px'});
            });
        });
    }
)(jQuery);